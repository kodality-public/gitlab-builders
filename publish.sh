#!/bin/sh
cd `dirname $0`
name=$1
cd $name
repo="registry.gitlab.com/kodality/gitlab-builders"

while read vers; do
  docker build -t $repo/$name . || exit 1
  docker tag $repo/$name $repo/$name:$vers || exit 1
  docker push $repo/$name:$vers || exit 1
done < versions


